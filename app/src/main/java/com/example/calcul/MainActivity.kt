package com.example.calcul

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {

    private lateinit var resultTextView: TextView
    private lateinit var resultTextView1: TextView

    private var operand: Double = 0.0
    private var operation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultTextView)
        resultTextView1 = findViewById(R.id.resultTextView1)

    }

    fun numberClick(clickedView: View) {

        if (clickedView is TextView) {

            var result = resultTextView.text.toString()
            val result1 = resultTextView1.text.toString()
            val number = clickedView.text.toString()

            if (result == "0") {
                result = ""
            }

            resultTextView.text = result + number
            resultTextView1.text = result + number
        }


    }

    fun operationClick(clickedView: View) {

        if (clickedView is TextView) {

            val result = resultTextView.text.toString()

            if (result.isNotEmpty()) {

                operand = result.toDouble()
            }

            operation = clickedView.text.toString()

            resultTextView.text = ""

            resultTextView1.text = resultTextView1.text.toString() + operation


        }

    }

    fun equalsClick(clickedView: View) {

        val secOperandText = resultTextView.text.toString()
        var secOperand: Double = 0.0

        if (secOperandText.isNotEmpty()) {
            secOperand = secOperandText.toDouble()

        }
        when (operation) {

            "+" -> resultTextView.text = (operand + secOperand).toString()
            "-" -> resultTextView.text = (operand - secOperand).toString()
            "*" -> resultTextView.text = (operand * secOperand).toString()
            "%" -> resultTextView.text = ((operand * secOperand) / 100).toString()
            "/" -> {

                if (secOperandText == "0") {

                    Toast.makeText(applicationContext, "Can't divide by zero.", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    resultTextView.text = (operand / secOperand).toString()
                }
            }

        }
        resultTextView1.text = ""

    }


    fun clearClick(clickedView: View) {
        resultTextView.text = ""
        resultTextView1.text = ""
    }

    fun clearOneNumberClick(clickedView: View) {
        resultTextView.text = resultTextView.text.toString().dropLast(1)
    }

    fun floutClick(clickedView: View) {

        var result = resultTextView.text.toString()

        if("." !in result && result.isNotEmpty()) {
            resultTextView.text = "$result."

        }

    }
    fun scrtClick(clickedView: View) {

        var result = resultTextView.text.toString()

        if (result.isNotEmpty()) {

            var sqrt = sqrt(result.toDouble())
            resultTextView.text = sqrt.toString()

        } else {
            Toast.makeText(applicationContext, "Enter number.", Toast.LENGTH_SHORT).show()
        }

    }



}